![picture](https://cdn2.steamgriddb.com/logo_thumb/e440113dde9d989caff2c0f405ad1334.png) 

﻿The Legend of Zelda: Ocarina of Time Powered by the soh reimplimentation engine 
This has no affiliation with the open source soh reimplimentation engine.

Repository for AUR package
Packages:
[soh-bin](https://aur.archlinux.org/packages/soh-bin))
[zeldaoot](https://aur.archlinux.org/packages/zeldaoot))

 ### Author
  * Corey Bruce
